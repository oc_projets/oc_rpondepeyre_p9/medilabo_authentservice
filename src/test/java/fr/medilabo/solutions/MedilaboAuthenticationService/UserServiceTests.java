package fr.medilabo.solutions.MedilaboAuthenticationService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;

import fr.medilabo.solutions.medilaboauthenticationservice.dtos.Subscriber;
import fr.medilabo.solutions.medilaboauthenticationservice.dtos.TokenInfos;
import fr.medilabo.solutions.medilaboauthenticationservice.models.UserModel;
import fr.medilabo.solutions.medilaboauthenticationservice.repositories.UserRepository;
import fr.medilabo.solutions.medilaboauthenticationservice.services.UserService;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository userRepository;
    @Mock
    PasswordEncoder encoder;

    @InjectMocks
    UserService userService;

    @Test
    public void verifyMail_Ok() {
        when(userRepository.findByMail("mail")).thenReturn(Optional.of(new UserModel()));
        Boolean result = this.userService.verifyMail("mail");

        assertTrue(result);
    }

    @Test
    public void verifyMail_Ko() {
        when(userRepository.findByMail("mail")).thenReturn(Optional.empty());
        Boolean result = this.userService.verifyMail("mail");

        assertFalse(result);
    }

    @Test
    public void subscribe_Ok() {
        when(userRepository.findByMail("mail")).thenReturn(Optional.empty());
        when(encoder.encode("password")).thenReturn("encodedPassword");
        Subscriber subscriber = new Subscriber();
        subscriber.setMail("mail");
        subscriber.setPassword("password");
        subscriber.setNom("nom");
        subscriber.setPrenom("prenom");

        userService.subscribe(subscriber, encoder);

        ArgumentCaptor<UserModel> captor = ArgumentCaptor.forClass(UserModel.class);
        verify(userRepository).save(captor.capture());

        UserModel verif = captor.getValue();

        assertThat(verif.getMail()).isEqualTo("mail");
        assertThat(verif.getPassword()).isEqualTo("encodedPassword");

    }

    @Test
    public void subscribe_MailTaken() {
        when(userRepository.findByMail("mail")).thenReturn(Optional.of(new UserModel()));
        Subscriber subscriber = new Subscriber();
        subscriber.setMail("mail");

        assertThrows(BadCredentialsException.class, () -> {
            userService.subscribe(subscriber, encoder);
        },
                "This mail is already taken");
    }

    @Test
    public void subscribe_NullPassword() {
        when(userRepository.findByMail("mail")).thenReturn(Optional.empty());
        Subscriber subscriber = new Subscriber();
        subscriber.setMail("mail");

        assertThrows(BadCredentialsException.class, () -> {
            userService.subscribe(subscriber, encoder);
        },
                "Invalid Password");
    }

    @Test
    public void subscribe_InvalidPassword() {
        when(userRepository.findByMail("mail")).thenReturn(Optional.empty());
        Subscriber subscriber = new Subscriber();
        subscriber.setMail("mail");
        subscriber.setPassword("");

        assertThrows(BadCredentialsException.class, () -> {
            userService.subscribe(subscriber, encoder);
        },
                "Invalid Password");
    }

    @Test
    public void TokenInfos_Ok() {
        UserModel user = new UserModel();
        user.setId(1);
        user.setMail("mail");
        user.setNom("nom");
        user.setPrenom("prenom");
        when(userRepository.findByMail("mail")).thenReturn(Optional.of(user));

        TokenInfos tokenInfos = userService.tokenInfos("mail");

        assertThat(tokenInfos.getId()).isEqualTo(1);
        assertThat(tokenInfos.getMail()).isEqualTo("mail");
        assertThat(tokenInfos.getNom()).isEqualTo("nom");
        assertThat(tokenInfos.getPrenom()).isEqualTo("prenom");
    }

    @Test
    public void TokenInfos_Ko() {
        when(userRepository.findByMail("mail")).thenReturn(Optional.empty());
        assertThrows(BadCredentialsException.class, () -> {
            userService.tokenInfos("mail");
        }, "Account not found");
    }

}
