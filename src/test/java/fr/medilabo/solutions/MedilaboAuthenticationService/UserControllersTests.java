package fr.medilabo.solutions.MedilaboAuthenticationService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import fr.medilabo.solutions.medilaboauthenticationservice.config.jwt.JwtTokenProvider;
import fr.medilabo.solutions.medilaboauthenticationservice.controllers.UserController;
import fr.medilabo.solutions.medilaboauthenticationservice.dtos.AuthRequest;
import fr.medilabo.solutions.medilaboauthenticationservice.dtos.Subscriber;
import fr.medilabo.solutions.medilaboauthenticationservice.dtos.Token;
import fr.medilabo.solutions.medilaboauthenticationservice.dtos.TokenInfos;
import fr.medilabo.solutions.medilaboauthenticationservice.services.UserService;

@ExtendWith(MockitoExtension.class)
public class UserControllersTests {

    @Mock
    UserService userService;
    @Mock
    AuthenticationManager authenticationManager;
    @Mock
    JwtTokenProvider jwtTokenProvider;
    @Mock
    PasswordEncoder encoder;
    @Mock
    Authentication authentication;
    @Mock
    SecurityContext securityContext;
    @Mock
    UserDetails userDetails;

    @InjectMocks
    UserController userController;

    @Test
    void mailExistTest() {
        when(userService.verifyMail("mail")).thenReturn(true);
        ResponseEntity<Boolean> result = userController.mailExist("mail");
        verify(userService).verifyMail("mail");
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isTrue();
    }

    @Test
    void subscribeTest() {
        Subscriber subscriber = new Subscriber();

        ResponseEntity<String> result = userController.subscribe(subscriber);
        verify(userService).subscribe(subscriber, encoder);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);

    }

    @Test
    void login() {
        when(authenticationManager.authenticate(any())).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(userDetails);
        when(jwtTokenProvider.generateToken(userDetails)).thenReturn("Token");

        ResponseEntity<Token> result = userController.login(new AuthRequest());

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody().getToken()).isEqualTo("Token");

    }

    @Test
    void getTokenInfoTest_Ok() {
        MockedStatic<SecurityContextHolder> securityContextHolder = Mockito.mockStatic(SecurityContextHolder.class);
        try {
            securityContextHolder.when(SecurityContextHolder::getContext).thenReturn(securityContext);
            when(securityContext.getAuthentication()).thenReturn(authentication);
            when(authentication.getPrincipal()).thenReturn(userDetails);
            when(userDetails.getUsername()).thenReturn("username");
            when(authentication.isAuthenticated()).thenReturn(true);
            when(userService.tokenInfos(anyString())).thenReturn(new TokenInfos());
            ResponseEntity<TokenInfos> result = userController.getTokenInfo();
            assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        } finally {
            securityContextHolder.close();
        }
    }

    @Test
    void getTokenInfo_Ko() {

        MockedStatic<SecurityContextHolder> securityContextHolder = Mockito.mockStatic(SecurityContextHolder.class);
        try {
            securityContextHolder.when(SecurityContextHolder::getContext).thenReturn(securityContext);
            when(securityContext.getAuthentication()).thenReturn(authentication);
            when(authentication.isAuthenticated()).thenReturn(false);
            ResponseEntity<TokenInfos> result = userController.getTokenInfo();
            assertThat(result.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        } finally {
            securityContextHolder.close();

        }
    }
}