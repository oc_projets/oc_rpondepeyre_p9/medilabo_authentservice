package fr.medilabo.solutions.medilaboauthenticationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedilaboAuthenticationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedilaboAuthenticationServiceApplication.class, args);
	}

}
