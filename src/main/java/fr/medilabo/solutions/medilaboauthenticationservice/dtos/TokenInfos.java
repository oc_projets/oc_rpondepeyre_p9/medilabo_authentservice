package fr.medilabo.solutions.medilaboauthenticationservice.dtos;

public class TokenInfos {

    private int id;
    private String nom;
    private String prenom;
    private String mail;

    public TokenInfos() {
        // Empty Constructor
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

}
