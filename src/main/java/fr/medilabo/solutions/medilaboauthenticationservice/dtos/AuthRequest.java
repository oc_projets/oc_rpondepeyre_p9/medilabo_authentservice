package fr.medilabo.solutions.medilaboauthenticationservice.dtos;

public class AuthRequest {
    private String mail;
    private String password;

    public AuthRequest() {
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
