package fr.medilabo.solutions.medilaboauthenticationservice.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.medilabo.solutions.medilaboauthenticationservice.config.jwt.JwtTokenProvider;
import fr.medilabo.solutions.medilaboauthenticationservice.dtos.AuthRequest;
import fr.medilabo.solutions.medilaboauthenticationservice.dtos.Subscriber;
import fr.medilabo.solutions.medilaboauthenticationservice.dtos.Token;
import fr.medilabo.solutions.medilaboauthenticationservice.dtos.TokenInfos;
import fr.medilabo.solutions.medilaboauthenticationservice.services.UserService;

@RestController
@RequestMapping("/auth")
public class UserController {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder encoder;

    public UserController(UserService userService, AuthenticationManager authenticationManager,
            JwtTokenProvider jwtTokenProvider, PasswordEncoder encoder) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.encoder = encoder;
    }

    @GetMapping("/mailexist")
    public ResponseEntity<Boolean> mailExist(@RequestParam(name = "mail") String mail) {
        return ResponseEntity.ok(userService.verifyMail(mail));
    }

    @PostMapping("/subscribe")
    public ResponseEntity<String> subscribe(@RequestBody Subscriber subscriber) {
        userService.subscribe(subscriber, encoder);
        return ResponseEntity.status(HttpStatus.CREATED).body("{\"message\": \"Account successfully created\"}");
    }

    @PostMapping("/login")
    public ResponseEntity<Token> login(@RequestBody AuthRequest authRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authRequest.getMail(), authRequest.getPassword()));

        UserDetails user = (UserDetails) authentication.getPrincipal();
        String token = jwtTokenProvider.generateToken(user);
        Token tokenResponse = new Token(token);
        return ResponseEntity.ok(tokenResponse);
    }

    @GetMapping("/tokeninfo")
    public ResponseEntity<TokenInfos> getTokenInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {
            UserDetails user = (UserDetails) authentication.getPrincipal();
            return ResponseEntity.ok(userService.tokenInfos(user.getUsername()));
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }
}
