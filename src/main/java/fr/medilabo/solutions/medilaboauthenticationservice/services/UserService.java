package fr.medilabo.solutions.medilaboauthenticationservice.services;

import java.util.Collections;
import java.util.Optional;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.medilabo.solutions.medilaboauthenticationservice.dtos.Subscriber;
import fr.medilabo.solutions.medilaboauthenticationservice.dtos.TokenInfos;
import fr.medilabo.solutions.medilaboauthenticationservice.models.UserModel;
import fr.medilabo.solutions.medilaboauthenticationservice.repositories.UserRepository;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public boolean verifyMail(String mail) {
        return this.repository.findByMail(mail).isPresent();
    }

    public void subscribe(Subscriber subscriber, PasswordEncoder encoder) {
        if (!this.verifyMail(subscriber.getMail())) {
            if (subscriber.getPassword() == null || subscriber.getPassword().isEmpty()) {
                throw new BadCredentialsException("Invalid Password");
            } else {
                UserModel user = new UserModel();
                user.setMail(subscriber.getMail());
                user.setNom(subscriber.getNom());
                user.setPrenom(subscriber.getPrenom());
                user.setPassword(encoder.encode(subscriber.getPassword()));
                repository.save(user);
            }
        } else {
            throw new BadCredentialsException("This mail is already taken");
        }
    }

    public TokenInfos tokenInfos(String mail) {
        Optional<UserModel> userBox = repository.findByMail(mail);
        if (!userBox.isPresent()) {
            throw new BadCredentialsException("Account not found");
        }
        TokenInfos tokenInfos = new TokenInfos();
        tokenInfos.setId(userBox.get().getId());
        tokenInfos.setMail(userBox.get().getMail());
        tokenInfos.setNom(userBox.get().getNom());
        tokenInfos.setPrenom(userBox.get().getPrenom());
        return tokenInfos;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserModel> userBox = repository.findByMail(username);

        if (!userBox.isPresent()) {
            throw new UsernameNotFoundException("Account not found");
        }

        return new User(userBox.get().getMail(), userBox.get().getPassword(),
                Collections.singleton(new SimpleGrantedAuthority("ROLE_USER")));

    }

}
