package fr.medilabo.solutions.medilaboauthenticationservice.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.medilabo.solutions.medilaboauthenticationservice.models.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Integer> {

    Optional<UserModel> findByMail(String mail);
}
